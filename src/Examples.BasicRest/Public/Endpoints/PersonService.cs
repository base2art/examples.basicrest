﻿namespace Examples.BasicRest.Public.Endpoints
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.Validation;
    using Examples.BasicRest.Public.Resources;
    
    public class PersonService
    {
        private static readonly KeyedCollection<Guid, Person> collection = new PersonCollection();

        public PersonService()
        {
        }
        
        //SEE CONFIG FOR URL
        public Task<Person[]> GetPeople()
        {
            return Task.FromResult(collection.ToArray());
        }
        
        public Task<Person> AddOrUpdatePerson(Person person)
        {
            person.Validate().IsNotNull();
            
            if (person.Id == Guid.Empty)
            {
                person.Id = Guid.NewGuid();
            }
            
            person.Name.Validate().IsNotNullEmptyOrWhiteSpace();
            
            collection.Add(person);
            return Task.FromResult(person);
        }
        
        public Task RemovePerson(Guid id)
        {
            collection.Remove(id);
            return Task.FromResult(true);
        }
        
        
        public class PersonCollection : KeyedCollection<Guid, Person>
        {
            protected override Guid GetKeyForItem(Person item)
            {
                return item.Id;
            }
        }
    }
}
