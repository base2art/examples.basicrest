﻿namespace Examples.BasicRest.Public.Resources
{
    using System;
    using System.Collections.Generic;

    public class Person
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}
