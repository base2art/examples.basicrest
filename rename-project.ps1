if (Test-Path "./.git/") {
  rm -recurse -force ./.git/
}

$company = Read-Host "Please enter your company"
$product = Read-Host "Please enter your product"
$newNamespace = "$($company).$($product)"

function RenameCurr($current) {
  if ($current.Name -like "*Examples.BasicRest*") {
    $parentFullName = $current.PSParentPath

    $fullName = $current.FullName
    $newName = $current.Name
    $newName = $newName -replace "Examples.BasicRest", $newNamespace

    Rename-Item -Path $fullName -NewName  $newName
    $newFullName = Join-Path $parentFullName $newName
    $current = Get-Item $newFullName
  }
  
  if ($current.PSIsContainer) {

    $children = Get-ChildItem -Path $current.FullName
    forEach ($child in $children) {
      RenameCurr $child
    }
  }
}


$currs = Get-ChildItem -Path .
forEach ($curr in $currs) {
  RenameCurr $curr
}

$rewrites = Get-ChildItem -Exclude *.ps1 -Recurse | Where-Object { !$_.PSIsContainer }


forEach ($rewrite in $rewrites) {
  $content = [IO.File]::ReadAllText($rewrite.FullName)
  #echo $content.ToLowerInvariant()
  if ($content.ToLowerInvariant().Contains("examples.basicrest")) {
    #echo $rewrite
    $content = $content -ireplace [regex]::Escape("examples.basicrest"), $newNamespace
    Set-Content -Path $rewrite -Value $content
  }
}



